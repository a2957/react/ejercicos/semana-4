
function primesEratostenes(n) {
    const p = [];
// Implementacion de la Criba de Eratóstenes
    for (let i = 2; i <= n; i++) {
        p.push(i)
    }
    for (let i = 2; i <= Math.floor(Math.sqrt(n)); i++) {
        for (let j = i; j <= Math.floor(n/i); j++) {
            const element = i*j;
            const position = p.indexOf(element);
            if (position !== -1) {
                p[position] = 0;
            }
        }      
    }

    return p.filter(value => value !== 0);
}

function sumPrimes(num) {
    total = primesEratostenes(num).reduce((subtotal, current) => subtotal + current);
    
    return total;
}

const result = sumPrimes(977);
console.log(result);
